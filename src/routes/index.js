const { Router } = require('express');

const routes = Router();

routes.use('/api/employees', require('./users'))
routes.use('/api/departments', require('./departments'))
routes.use('/api/roles', require('./roles'))

module.exports = routes;