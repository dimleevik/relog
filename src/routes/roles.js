const { Router } = require('express');

const RolesController = require('../controllers/RolesController');

const routes = Router();

routes.get('/', RolesController.all)
routes.get('/:id', RolesController.getByID)
routes.post('/', RolesController.getByID)

module.exports = routes;