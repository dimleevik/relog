db.createCollection('Departments');
db.createCollection('Employee');
db.createCollection('Roles')

db.Departments.insertMany([{
  name: 'Sales'
}, {
  name: 'IT'
}, {
  name: 'Head'
}, {
  name: 'Support'
}])

db.Roles.insertMany([{
  name: 'sales manager'
}, {
  name: 'director'
}, {
  name: 'worker'
}, {
  name: 'lead manager'
}, {
  name: 'developer'
}, {
  name: 'manager'
}])

