const mongoose = require('mongoose');
const { Schema } = mongoose;

const employeeSchema = new Schema({
  salary: Number,
  department: {
    type: Schema.Types.ObjectId,
    ref: 'Departments',
  },
  role: {
    type: Schema.Types.ObjectId,
    ref: 'Roles',
  },
  age: Number,
  name: String
});

const Employee = mongoose.model('Employee', employeeSchema, 'Employees');

module.exports = Employee
