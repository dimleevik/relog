const mongoose = require('mongoose');

const { Department } = require('../models');
const ObjectId = mongoose.Types.ObjectId;

module.exports = class DepartmentController {
  static async all(req, res) {
    try {
      const departments = await Department.aggregate([{
        $lookup:
        {
          from: "Employees",
          localField: "_id",
          foreignField: "department",
          as: "employees"
        }
      }])

      res.send(departments);
    } catch (e) {
      res.status(500).json(e);
    }
  }
  static async findById(req, res) {
    try {
      const result = await Department.aggregate([{
        $lookup:
        {
          from: "Employees",
          localField: "_id",
          foreignField: "department",
          as: "employees"
        }
      }, { $match: { _id: ObjectId(req.params.id) } }])
      res.send(result);
    } catch (e) {
      console.log(e);
      res.status(500).json(e);
    }
  }

  static async create(req, res) {
    try {
      const { name } = req.body
      const exists = await Department.findOne({ name });
      if (exists) {
        console.log(exists)
        res.status(400).json({ message: 'exists' })
        return
      }
      const department = await Department.create({ name })
      res.send(department);
    } catch (e) {
      console.log(e);
      res.status(500).json({ message: 'Internal error' })
    }
  }

}