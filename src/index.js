require('dotenv').config()

const DB = require('./db');
const { Department } = require('./models');
const routers = require('./routes');
const HttpService = require('./http');

const http = new HttpService(routers);

http.start();
DB.start();