const mongoose = require('mongoose');
const { Schema } = mongoose;

const departmentSchema = new Schema({
  name: String,
});

const Department = mongoose.model('Department', departmentSchema, 'Departments');
module.exports = Department
