const mongoose = require('mongoose');

const url = `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@localhost:27017/${process.env.MONGO_DB}?authSource=admin`

module.exports = class DB {
  static start() {
    mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })
  }
}
