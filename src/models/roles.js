const mongoose = require('mongoose');
const { Schema } = mongoose;

const rolesSchema = new Schema({
  id: Number,
  name: String
});

const Role = mongoose.model('Role', rolesSchema, 'Roles');

module.exports = Role