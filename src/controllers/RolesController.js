const mongoose = require('mongoose');

const { Role } = require('../models');
const ObjectId = mongoose.Types.ObjectId;

module.exports = class RoleController {
  static async all(req, res) {
    try {
      const result = await Role.aggregate([{
        $lookup:
        {
          from: "Employees",
          localField: "_id",
          foreignField: "role",
          as: "employees"
        }
      }])
      res.send(result);
    } catch (e) {
      res.status(500).json(e);
    }
  }
  static async getByID(req, res) {
    try {
      const result = await Role.aggregate([{
        $lookup:
        {
          from: "Employees",
          localField: "_id",
          foreignField: "role",
          as: "employees"
        }
      }, { $match: { _id: ObjectId(req.params.id) } }])
      res.send(result);
    } catch (e) {
      res.status(500).json(e);
    }
  }
  static async create(req, res) {
    try {
      const { name } = req.body
      const exists = await Role.findOne({ name });
      if (exists) {
        console.log(exists)
        res.status(400).json({ message: 'exists' })
        return
      }

      const role = await Role.create({ name })
      res.send(role);
    } catch (e) {
      console.log(e);
      res.status(500).json({ message: 'Internal error' })
    }
  }
}