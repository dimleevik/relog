const express = require('express');
const app = express();
module.exports = class HttpService {

  constructor(routes) {
    app.use(express.json())
    app.use(routes);
  }

  start() {
    app.listen(process.env.PORT, () => {
      console.log(`listen on ${process.env.PORT}`)
    });
  }
}