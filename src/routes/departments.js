const { Router } = require('express');

const DepartmentController = require('../controllers/DepartmentController');

const routes = Router();

routes.get('/', DepartmentController.all)
routes.get('/:id', DepartmentController.findById)
routes.post('/', DepartmentController.create)


module.exports = routes;