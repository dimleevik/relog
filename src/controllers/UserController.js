const mongoose = require('mongoose');

const { Employee, Department, Role } = require('../models');
const ObjectId = mongoose.Types.ObjectId;

module.exports = class UserController {
  static async all(req, res) {
    try {
      const employees = await Employee.aggregate([{
        $lookup:
        {
          from: "Departments",
          localField: "department",
          foreignField: "_id",
          as: "department"
        }
      }, {
        $lookup:
        {
          from: "Roles",
          localField: "role",
          foreignField: "_id",
          as: "role"
        }
      }])
      res.send(employees);
    } catch (error) {
      console.log(error);
    }
  }

  static async byId(req, res) {
    try {
      const employees = await Employee.aggregate([{
        $lookup:
        {
          from: "Departments",
          localField: "department",
          foreignField: "_id",
          as: "department"
        }
      }, {
        $lookup:
        {
          from: "Roles",
          localField: "role",
          foreignField: "_id",
          as: "role"
        }
      }, {
        $match: {
          _id: ObjectId(req.params.id)
        }
      }])
      res.send(employees);
    } catch (error) {
      console.log(error);
    }
  }

  static async create(req, res) {
    try {
      const { name, age, salary, departmentId: department, roleId: role } = req.body
      const [isDept, isRole] = await Promise.all([Department.findById(department),
      Role.findById(role)]);

      if (!isRole || !isDept) {
        res.status(400).json({ message: 'Not correct Role/Department' })
      }

      const employee = await Employee.create({ name, age, salary, department, role })
      res.send(employee);
    } catch (e) {
      console.log(e);
      res.status(500).json({ message: 'Internal error' })
    }
  }

  static async delete(req, res) {
    try {
      const employee = await Employee.deleteOne({ _id: req.params.id });
      if (employee?.deletedCount) {
        res.send(true)
        return
      }
      res.status(400).json({ message: 'not found' })
    } catch (error) {
      res.status(500).json(error);
    }
  }
}