const { Router } = require('express');

const UserController = require('../controllers/UserController');

const routes = Router();

routes.get('/', UserController.all)
routes.get('/:id', UserController.byId)
routes.post('/', UserController.create)
routes.delete('/:id', UserController.delete)


module.exports = routes;