module.exports = {
  Role: require('./roles'),
  Employee: require('./employees'),
  Department: require('./departments')
}